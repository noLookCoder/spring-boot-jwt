package com.krawczyk.maciej.authentication.service;

import org.apache.commons.lang3.SerializationUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class UserService implements UserDetailsService {
    private static final String USER_NOT_FOUND_MSG = "User with name '%s' not found.";

    private final List<User> users = new ArrayList<>();

    @Value("${mock.username}")
    private String name;

    @Value("${mock.password}")
    private String password;

    @PostConstruct
    public void mockUsers() {
        users.add(new User(name, password, Collections.emptyList()));
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        return users.stream()
                .filter(user -> user.getUsername().equals(username))
                .map(SerializationUtils::clone)
                .findAny()
                .orElseThrow(() -> new UsernameNotFoundException(String.format(USER_NOT_FOUND_MSG, username)));
    }
}
