package com.krawczyk.maciej.authentication.dto;

import java.io.Serializable;

public class JwtResponse implements Serializable {
    private static final long serialVersionUID = 1L;

    private final String content;

    public JwtResponse(String content) {
        this.content = content;
    }

    public String getToken() {
        return content;
    }
}
