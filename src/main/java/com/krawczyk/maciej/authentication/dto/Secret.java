package com.krawczyk.maciej.authentication.dto;

import java.util.Objects;

public class Secret {
    private String value;

    public Secret(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if(this == o) {
            return true;
        }
        if(!(o instanceof Secret)) {
            return false;
        }
        Secret secret = (Secret) o;
        return Objects.equals(getValue(), secret.getValue());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getValue());
    }

    @Override
    public String toString() {
        return "Secret{" +
                "value='" + value + '\'' +
                '}';
    }
}
