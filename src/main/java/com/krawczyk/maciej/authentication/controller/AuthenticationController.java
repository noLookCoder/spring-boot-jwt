package com.krawczyk.maciej.authentication.controller;

import com.krawczyk.maciej.authentication.dto.JwtRequest;
import com.krawczyk.maciej.authentication.dto.JwtResponse;
import com.krawczyk.maciej.authentication.service.JwtService;
import com.krawczyk.maciej.authentication.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class AuthenticationController {
    public static final String AUTHENTICATE_ENDPOINT = "/authenticate";

    private final JwtService jwtService;
    private final UserService userService;
    private final AuthenticationManager authenticationManager;

    @Autowired
    public AuthenticationController(JwtService jwtService, UserService userService,
                                    AuthenticationManager authenticationManager) {
        this.jwtService = jwtService;
        this.userService = userService;
        this.authenticationManager = authenticationManager;
    }

    @PostMapping(AUTHENTICATE_ENDPOINT)
    public ResponseEntity<JwtResponse> authenticate(@RequestBody JwtRequest request) {
        String username = request.getUsername();
        String password = request.getPassword();
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        UserDetails userDetails = userService.loadUserByUsername(username);
        String token = jwtService.generateToken(userDetails);
        return ResponseEntity.ok(new JwtResponse(token));
    }
}
