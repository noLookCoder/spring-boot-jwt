package com.krawczyk.maciej.authentication.controller;

import com.krawczyk.maciej.authentication.dto.Secret;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;

@RestController
public class SecretController {

    @GetMapping("/secrets")
    public ResponseEntity<List<Secret>> getSecret() {
        return ResponseEntity.ok(Collections.singletonList(new Secret("Visible only for authenticated users.")));
    }
}
