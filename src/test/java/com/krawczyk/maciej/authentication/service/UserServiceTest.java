package com.krawczyk.maciej.authentication.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {
    private static final String EXISTING_USERNAME = "admin";
    private static final String EXISTING_PASSWORD = "$2a$10$FesxbHzm1CBn1Pb5Ltsrd.Sslz0e.ywgEfQt2tQFLBADrKbVCLP/e";

    @Autowired
    private UserService userService;

    @Test(expected = UsernameNotFoundException.class)
    public void shouldThrowExceptionWhenUserDoesNotExist() {
        userService.loadUserByUsername("invalid username");
    }

    @Test
    public void shouldReturnUserWhenUserWithGivenNameExists() {
        //when
        UserDetails userDetails = userService.loadUserByUsername(EXISTING_USERNAME);

        //then
        assertThat(userDetails.getUsername()).isEqualTo(EXISTING_USERNAME);
        assertThat(userDetails.getPassword()).isEqualTo(EXISTING_PASSWORD);
    }
}