package com.krawczyk.maciej.authentication.service;

public class AnotherUserDetailsStub extends UserDetailsStub {

    @Override
    public String getUsername() {
        return "foo";
    }
}
