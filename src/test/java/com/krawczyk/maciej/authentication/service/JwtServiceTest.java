package com.krawczyk.maciej.authentication.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class JwtServiceTest {

    @Autowired
    private JwtService jwtService;

    @Test
    public void shouldInvalidateTokenRequestedForAnotherUser() {
        //given
        UserDetails userDetails = new UserDetailsStub();
        UserDetails anotherUserDetails = new AnotherUserDetailsStub();

        //when
        String token = jwtService.generateToken(userDetails);

        //then
        assertThat(jwtService.isValid(token, anotherUserDetails)).isFalse();
    }

    @Test
    public void shouldValidateTokenRequestedForSameUser() {
        //given
        UserDetails userDetails = new UserDetailsStub();

        //when
        String token = jwtService.generateToken(userDetails);

        //then
        assertThat(jwtService.isValid(token, userDetails)).isTrue();
    }
}