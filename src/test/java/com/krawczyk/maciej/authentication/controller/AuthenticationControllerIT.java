package com.krawczyk.maciej.authentication.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static com.krawczyk.maciej.authentication.util.TestResourceReader.readAsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AuthenticationControllerIT {
    private static final String INVALID_AUTHENTICATION_REQUEST = readAsString("invalid-authentication-request.json");
    private static final String VALID_AUTHENTICATION_REQUEST = readAsString("valid-authentication-request.json");

    @Autowired
    private MockMvc mvc;

    @Test
    public void shouldReturnUnauthorizedForBadCredentials() throws Exception {
        mvc.perform(post("/authenticate")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(INVALID_AUTHENTICATION_REQUEST))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void shouldJwtForCorrectCredentials() throws Exception {
        mvc.perform(post("/authenticate")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(VALID_AUTHENTICATION_REQUEST))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$").isNotEmpty());
    }
}