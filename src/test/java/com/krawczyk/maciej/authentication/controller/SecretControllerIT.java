package com.krawczyk.maciej.authentication.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static com.krawczyk.maciej.authentication.util.TestResourceReader.readAsString;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class SecretControllerIT {
    private static final String VALID_TOKEN = readAsString("valid-token.txt");

    @Autowired
    private MockMvc mvc;

    @Test
    public void shouldReturnUnauthorizedWhenJwtIsMissing() throws Exception {
        mvc.perform(get("/secrets"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void shouldReturnSecretWhenJwtIsValid() throws Exception {
        mvc.perform(get("/secrets").header("Authorization", VALID_TOKEN))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].value").value("Visible only for authenticated users."));
    }
}