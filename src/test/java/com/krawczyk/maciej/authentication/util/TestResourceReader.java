package com.krawczyk.maciej.authentication.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class TestResourceReader {
    private static final String TEST_RESOURCES_PATH = "src/test/resources/";

    private TestResourceReader() {
    }

    public static String readAsString(String fileName) {
        try {
            return new String(Files.readAllBytes(Paths.get(TEST_RESOURCES_PATH + fileName))).trim();
        } catch(IOException e) {
            throw new RuntimeException(e);
        }
    }
}
