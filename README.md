# Demo project for Spring Boot + JWT

## Usage
### Step 1:
send HTTP POST request to `/authenticate` endpoint with following body:
```
{
  "username":"admin",
  "password":"admin123"
}
```

### Step 2:
send HTTP GET request to `/secrets` with header:
```
Authorization: [Bearer] <token-from-step-1>
```
